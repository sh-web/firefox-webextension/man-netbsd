# NetBSD Manpage Search

A Firefox WebExtension that allows the user to search manual page into official NetBSD manual page server.
(man.netbsd.org/netbsd.gw.com)

- https://addons.mozilla.org/addon/netbsd-manpage/
